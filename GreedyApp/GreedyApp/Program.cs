﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GreedyApp.DTO;

namespace GreedyApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var grades = new List<Student>
            {
                new Student() {Name = "Max Brundige", Marks = 800},
                new Student() {Name = "Maddie Ijams", Marks = 900},
                new Student() {Name = "Caleb Bush", Marks = 700}
            };

            // Here we are performing a simple query to find the students that have marks above 750.
            // The key question is where does query get executed?
            var result = grades.Where(x => x.Marks > 750);

            // The query gets executed in our foreach loop below as we are iterate through the sequence. This is called deferred execution.
            // Again, we are not executing the linq statement where is is defined above until we iterate below. Therefore, it is deferred.
            Console.WriteLine("Starting the first deferred execution sequence");
            foreach (var student in result)
            {
                Console.WriteLine(student.Name + "\t" + student.Marks);
            }
            Console.WriteLine("---------------------------------------------------------------------");
            // In LINQ, there are two types of operations:
            // 1. Deferred Operations - things like where, select
            // 2. Immediate or greedy operations - things like ToList and Count
            // Are you still not believing me? Or you may be saying, "Screw it... I am just going to write to a list every time. 
            // Be careful my friend. Let's say we need to add another student to our collection above.
            // As you will notice, adding a student AFTER the linq query above does not cause this wonderful student to miss out.

            grades.Add(new Student() {Name= "Iesha Duff", Marks = 1000 });

            Console.WriteLine("Starting the second deferred execution sequence with our new student");
            foreach (var student in result)
            {
                Console.WriteLine(student.Name + "\t" + student.Marks);
            }
            Console.WriteLine("---------------------------------------------------------------------");

            // Yay... now let's see how Immediate execution works with the greedy operator - ToList().

            var greedyResult = grades.Where(x => x.Marks > 750).ToList();

            grades.Add(new Student() { Name = "Justin Bolles", Marks = 751 });

            Console.WriteLine("Starting the first greedy sequence with our new student");
            foreach (var student in greedyResult)
            {
                Console.WriteLine(student.Name + "\t" + student.Marks);
            }
            Console.WriteLine("---------------------------------------------------------------------");

            // As you see above, Justin had the marks to pass, but with immediate execution, the execution ran where it was defined.
            // So, like every programmer, you are asking, "When do I use deferred vs immediate execution?"
            // And like most StackOverflow articles will kindly (or not so) tell you, it depends.
            //
            // Scenario 1: Doing multiple "things" to the data you return from the database
            // If you are running multiple operations on the data you receive back, it may be best to cache the result and use immediate execution.
            // In the example above, we iterated over the same sequence twice which caused use to re-execute the query again. This can be rather
            // heavy, if the results you are bringing back are large or require many complex joins, etc. Leading us to favor immediate execution.
            // 
            // Scenario 2: The data is changing at the speed of light
            // If the the data changes rapidly, it is likely best to use deferred execution to run additional operations on the latest snapshot
            // of the data.

        }
    }
}
