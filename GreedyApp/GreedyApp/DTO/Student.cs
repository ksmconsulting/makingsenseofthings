﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace GreedyApp.DTO
{
    class Student
    {
       public string Name { get; set; }
       public int Marks { get; set; }
    }
}
